# Movie Search App

This is a simple movie search app built with React. It allows users to search for movies or TV series using the Open Movie Database (OMDb) API and displays the search results.

## Features

- Search movies and TV series by title
- Display search results with movie details
- Click on a movie to view more information

## Installation

Clone the repository:
git clone https://github.com/Maneth-ii/movie-search-app.git

Install dependencies:
cd movie-search-app
npm install

## Obtain an API key from OMDb API by signing up for an account.

Create a .env file in the root directory and add your API key:

REACT_APP_API_KEY=your-api-key


Start the development server:
npm start

#Usage
>Enter a movie or TV series title in the search box and press Enter or click the search button.
>The search results will be displayed as a list of movie cards.
>Click on a movie card to view more details about the movie.

#Technologies Used
- React
- Axios
- HTML
- CSS

#Contributing
Contributions are welcome! If you find any issues or have suggestions for improvement, feel free to open an issue or submit a pull request.
